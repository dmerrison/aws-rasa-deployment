build:
npm run build

synth: build
cdk synth

deploy: build
cdk deploy