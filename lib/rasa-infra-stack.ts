import { Vpc } from '@aws-cdk/aws-ec2';
import { TaskDefinition, Cluster, ContainerImage, Compatibility, Protocol } from '@aws-cdk/aws-ecs';
import { CfnOutput, Stack, StackProps, Construct, Duration } from '@aws-cdk/core';
import { ApplicationLoadBalancedFargateService } from '@aws-cdk/aws-ecs-patterns';

export class RasaInfraStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, {
      env: {
        region: "eu-west-2",
      }
    });

    const vpc = new Vpc(this, "RasaVPC", {
      maxAzs: 2,
      natGateways: 1,
    })

    const cluster = new Cluster(this, "RasaCluster", {
      vpc: vpc,
    })
    
    const rasaTaskDefinition = new TaskDefinition(this, "RasaTaskDefinition", {
      cpu: '256',
      memoryMiB: '512',
      compatibility: Compatibility.FARGATE,
    })

    const rasaContainer = rasaTaskDefinition.addContainer("RasaContainer", {
      image: ContainerImage.fromAsset("./docker/rasa"),
      command: [
        "run",
      ],
    })

    rasaContainer.addPortMappings({
      containerPort: 5005,
      protocol: Protocol.TCP,
    })
    
    const rasaSvc = new ApplicationLoadBalancedFargateService(this, "RasaService", {
        cluster: cluster,
        taskDefinition: rasaTaskDefinition,
        publicLoadBalancer: true,    
    }) 
    
    const rasaAutoScalingGroup = rasaSvc.service.autoScaleTaskCount({
      minCapacity: 2,
      maxCapacity: 10,
    })

    rasaAutoScalingGroup.scaleOnCpuUtilization("RasaScaleOnCPU", {
      targetUtilizationPercent: 70,
      scaleOutCooldown: Duration.seconds(180),
    })

    // Output a test curl command constructed using the output of the loadbalancer dns name
    new CfnOutput(this, "OutputTestRequest", {
      exportName: "OutputTestRequest",
      value: `curl -XPOST http://${rasaSvc.loadBalancer.loadBalancerDnsName}/webhooks/rest/webhook -H "Content-type: application/json" -d '{"sender": "test", "message": "hello"}'`
    })

  }
}
