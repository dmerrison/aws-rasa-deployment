#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { RasaInfraStack } from '../lib/rasa-infra-stack';

const app = new cdk.App();
new RasaInfraStack(app, 'RasaInfraStack');
